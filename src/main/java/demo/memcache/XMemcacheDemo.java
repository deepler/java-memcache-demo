package demo.memcache;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientBuilder;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.exception.MemcachedException;
import net.rubyeye.xmemcached.utils.AddrUtil;

public class XMemcacheDemo {
	public static void main(String[] args) {
		MemcachedClientBuilder builder = 
				new XMemcachedClientBuilder(AddrUtil.getAddresses("localhost:11211"));
		MemcachedClient memcachedClient = null;
		try {
			memcachedClient = builder.build();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			// 第一个参数：key。第二个参数：单位是秒，意思是存储时间，0为永久
			// 第三个参数：value
			memcachedClient.set("hello", 5, "Hello,xmemcached");

			String value = (String) memcachedClient.get("hello");
			System.out.println("hello=" + value);
			
			//等待6秒后存储失效
			Thread.sleep(6*1000);
			//memcachedClient.delete("hello");
			value = (String) memcachedClient.get("hello");
			System.out.println("hello=" + value);

		} catch (MemcachedException e) {
			System.err.println("MemcachedClientoperation fail");
			e.printStackTrace();
		} catch (TimeoutException e) {
			System.err.println("MemcachedClientoperation timeout");
			e.printStackTrace();
		} catch (InterruptedException e) {
		}
		try {
			memcachedClient.shutdown();
		} catch (IOException e) {
			System.err.println("ShutdownMemcachedClient fail");
			e.printStackTrace();
		}

	}
}
